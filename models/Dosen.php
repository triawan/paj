<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dosen".
 *
 * @property int $id
 * @property string $nidn
 * @property string $nama
 * @property string $jeniskelamin
 * @property string $prodi
 * @property string $statuskeaktifan
 * @property string $statuskepegawaian
 * @property string $statusperkawinan
 * @property string $nomorsk
 * @property string $tmmd
 */
class Dosen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dosen';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db4');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'nidn', 'nama', 'jeniskelamin', 'prodi', 'statuskeaktifan', 'statuskepegawaian', 'statusperkawinan', 'nomorsk', 'tmmd'], 'required'],
            [['id'], 'integer'],
            [['jeniskelamin', 'statuskeaktifan', 'statuskepegawaian', 'statusperkawinan'], 'string'],
            [['tmmd'], 'safe'],
            [['nidn'], 'string', 'max' => 15],
            [['nama', 'nomorsk'], 'string', 'max' => 50],
            [['prodi'], 'string', 'max' => 20],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nidn' => 'Nidn',
            'nama' => 'Nama',
            'jeniskelamin' => 'Jeniskelamin',
            'prodi' => 'Prodi',
            'statuskeaktifan' => 'Statuskeaktifan',
            'statuskepegawaian' => 'Statuskepegawaian',
            'statusperkawinan' => 'Statusperkawinan',
            'nomorsk' => 'Nomorsk',
            'tmmd' => 'Tmmd',
        ];
    }
}
