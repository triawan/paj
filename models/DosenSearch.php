<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Dosen;

/**
 * DosenSearch represents the model behind the search form of `app\models\Dosen`.
 */
class DosenSearch extends Dosen
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['nidn', 'nama', 'jeniskelamin', 'prodi', 'statuskeaktifan', 'statuskepegawaian', 'statusperkawinan', 'nomorsk', 'tmmd'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Dosen::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tmmd' => $this->tmmd,
        ]);

        $query->andFilterWhere(['like', 'nidn', $this->nidn])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'jeniskelamin', $this->jeniskelamin])
            ->andFilterWhere(['like', 'prodi', $this->prodi])
            ->andFilterWhere(['like', 'statuskeaktifan', $this->statuskeaktifan])
            ->andFilterWhere(['like', 'statuskepegawaian', $this->statuskepegawaian])
            ->andFilterWhere(['like', 'statusperkawinan', $this->statusperkawinan])
            ->andFilterWhere(['like', 'nomorsk', $this->nomorsk]);

        return $dataProvider;
    }
}
