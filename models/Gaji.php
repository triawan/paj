<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gaji".
 *
 * @property int $id
 * @property string $golongan
 * @property string $nomorsk
 * @property string $tanggalsk
 * @property int $gajipokok
 * @property string $tanggalmulai
 * @property string $tanggalselesai
 */
class Gaji extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gaji';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db5');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'golongan', 'nomorsk', 'tanggalsk', 'gajipokok', 'tanggalmulai', 'tanggalselesai'], 'required'],
            [['id', 'gajipokok'], 'integer'],
            [['tanggalsk', 'tanggalmulai', 'tanggalselesai'], 'safe'],
            [['golongan'], 'string', 'max' => 5],
            [['nomorsk'], 'string', 'max' => 50],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'golongan' => 'Golongan',
            'nomorsk' => 'Nomorsk',
            'tanggalsk' => 'Tanggalsk',
            'gajipokok' => 'Gajipokok',
            'tanggalmulai' => 'Tanggalmulai',
            'tanggalselesai' => 'Tanggalselesai',
        ];
    }
}
