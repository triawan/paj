<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "krs".
 *
 * @property int $id
 * @property int $id_semester
 * @property string $nim
 * @property string $kode_mk
 * @property string $nilai
 */
class Krs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'krs';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db1');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_semester', 'nim', 'kode_mk', 'nilai'], 'required'],
            [['id', 'id_semester'], 'integer'],
            [['nim'], 'string', 'max' => 15],
            [['kode_mk'], 'string', 'max' => 10],
            [['nilai'], 'string', 'max' => 1],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_semester' => 'Id Semester',
            'nim' => 'Nim',
            'kode_mk' => 'Kode Mk',
            'nilai' => 'Nilai',
        ];
    }
}
