<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mahasiswa".
 *
 * @property int $id
 * @property string $nim
 * @property string $nama
 * @property string $alamat
 * @property string $tempat_lahir
 * @property string $jenis_kelamin
 * @property string $agama
 * @property string $kode_jurusan
 */
class Mahasiswa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mahasiswa';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db2');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'nim', 'nama', 'alamat', 'tempat_lahir', 'jenis_kelamin', 'agama', 'kode_jurusan'], 'required'],
            [['id'], 'integer'],
            [['nim', 'agama'], 'string', 'max' => 15],
            [['nama', 'alamat', 'tempat_lahir'], 'string', 'max' => 50],
            [['jenis_kelamin'], 'string', 'max' => 1],
            [['kode_jurusan'], 'string', 'max' => 10],
            [['nim'], 'unique'],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nim' => 'Nim',
            'nama' => 'Nama',
            'alamat' => 'Alamat',
            'tempat_lahir' => 'Tempat Lahir',
            'jenis_kelamin' => 'Jenis Kelamin',
            'agama' => 'Agama',
            'kode_jurusan' => 'Kode Jurusan',
        ];
    }
}
