<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "matakuliah".
 *
 * @property int $id
 * @property string $kode_mk
 * @property string $nama_mk
 * @property int $sks
 * @property int $semester
 * @property string $jenis
 */
class Matakuliah extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'matakuliah';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db3');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'kode_mk', 'nama_mk', 'sks', 'semester', 'jenis'], 'required'],
            [['id', 'sks', 'semester'], 'integer'],
            [['kode_mk'], 'string', 'max' => 10],
            [['nama_mk'], 'string', 'max' => 50],
            [['jenis'], 'string', 'max' => 1],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kode_mk' => 'Kode Mk',
            'nama_mk' => 'Nama Mk',
            'sks' => 'Sks',
            'semester' => 'Semester',
            'jenis' => 'Jenis',
        ];
    }
}
