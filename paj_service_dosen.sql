-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 07, 2021 at 01:42 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `paj_service_dosen`
--

-- --------------------------------------------------------

--
-- Table structure for table `dosen`
--

CREATE TABLE `dosen` (
  `id` int(10) NOT NULL,
  `nidn` char(15) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jeniskelamin` set('Laki-Laki','Perempuan','','') NOT NULL,
  `prodi` varchar(20) NOT NULL,
  `statuskeaktifan` set('Aktif','Tidak Aktif','','') NOT NULL,
  `statuskepegawaian` set('PNS','Non PNS','','') NOT NULL,
  `statusperkawinan` set('Kawin','Tidak Kawin','','') NOT NULL,
  `nomorsk` char(50) NOT NULL,
  `tmmd` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `dosen`
--

INSERT INTO `dosen` (`id`, `nidn`, `nama`, `jeniskelamin`, `prodi`, `statuskeaktifan`, `statuskepegawaian`, `statusperkawinan`, `nomorsk`, `tmmd`) VALUES
(1, '0408078101', 'Agus Mulyana', 'Laki-Laki', 'Teknik Komputer', 'Aktif', 'Non PNS', 'Kawin', 'SK/110/10/2021', '2004-10-07'),
(2, '0427118904', 'Sopian Alviana', 'Laki-Laki', 'Teknik Informatika', 'Aktif', 'PNS', 'Kawin', 'SK/111/10/2021', '2004-10-08'),
(3, '2001128101', 'Defiana Arnaldy', 'Laki-Laki', 'Teknik Informatika', 'Aktif', 'PNS', 'Kawin', 'SK/112/10/2021', '2004-10-09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
