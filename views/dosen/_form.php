<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Dosen */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dosen-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'nidn')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jeniskelamin')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'prodi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'statuskeaktifan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'statuskepegawaian')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'statusperkawinan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nomorsk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tmmd')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
