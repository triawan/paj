<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\DosenSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dosen-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'nidn') ?>

    <?= $form->field($model, 'nama') ?>

    <?= $form->field($model, 'jeniskelamin') ?>

    <?= $form->field($model, 'prodi') ?>

    <?php // echo $form->field($model, 'statuskeaktifan') ?>

    <?php // echo $form->field($model, 'statuskepegawaian') ?>

    <?php // echo $form->field($model, 'statusperkawinan') ?>

    <?php // echo $form->field($model, 'nomorsk') ?>

    <?php // echo $form->field($model, 'tmmd') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
