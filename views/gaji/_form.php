<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Gaji */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gaji-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'golongan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nomorsk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tanggalsk')->textInput() ?>

    <?= $form->field($model, 'gajipokok')->textInput() ?>

    <?= $form->field($model, 'tanggalmulai')->textInput() ?>

    <?= $form->field($model, 'tanggalselesai')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
