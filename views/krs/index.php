<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KrsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Krs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="krs-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Krs', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_semester',
            'nim',
            'kode_mk',
            'nilai',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
